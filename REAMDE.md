# CUSTOM LETSENCRYPT RENEWAL



* 4096 bit keys
* tests for /etc/letsencrypt/renewal-hooks.d/YOURDOMAN.LAN and runs it on renewal
* tries to find your email adress automatically
  - uses the content of /etc/letsencrypt/.letsencrypt_default_mail as mail address
  - will fall back to the alphapetical first match of ANY domain you alreary have with letsencrypt

## installing:

 - `git clone https://gitlab.com/the-foundation/letsencrypt-custom-renewal.git /etc/custom/letsencrypt-custom-renewal`
 -  add a line to crontab 
    ```
    52 0,12 * * *   /bin/bash /etc/custom/letsencrypt-custom-renewal/custom-renew.sh &>/dev/shm/renew.cron.log
    ```

## testing

you can pass --dry-run and --force ( force will add --force-renewal to certbot arguments)
