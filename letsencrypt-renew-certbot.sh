#!/bin/bash
DRY=""
FORCE=""

get_first_letsencrypt_mail_address() {
         grep -h "@" $(ls -1 /etc/letsencrypt/accounts/*/directory/*/regr.json|grep -v staging)|sed 's/.\+mailto://g'|cut -d'"' -f1|sort -u |grep @|head -n1
}

[[ -z "$LETSENCRYPT_MAIL" ]] &&  LETSENCRYPT_MAIL=$(get_first_letsencrypt_mail_address)
[[ -z "$LETSENCRYPT_MAIL" ]] &&  LETSENCRYPT_MAIL=$(cat /etc/letsencrypt/.letsencrypt_default_mail 2>/dev/null |grep @)
[[ -z "$LETSENCRYPT_MAIL" ]] &&  LETSENCRYPT_MAIL=default-mail-not-set@mailsire.com

for i in "$@" ; do [[ $i == "--dry" ]]   && DRY=" --dry-run "         && break ; done
for i in "$@" ; do [[ $i == "--force" ]] && FORCE=" --force-renewal " && break ; done

## you have to have a executable file OR bash script at  /etc/letsencrypt/renewal-hooks.d/YOUR.DOMAIN
test -d /etc/letsencrypt/renewal-hooks.d || mkdir -p /etc/letsencrypt/renewal-hooks.d

find /etc/letsencrypt/live -maxdepth 1  -type d|grep -v ^/etc/letsencrypt/live$|while read a ;do
        certname=$(basename $a);
        echo -n ":RENEW " $certname
        test -f /etc/letsencrypt/renewal-hooks.d/$certname && ( 
                echo deploy hook found ;
	        chmod +x /etc/letsencrypt/renewal-hooks.d/$certname;
		/usr/bin/certbot renew --no-random-sleep-on-renew --agree-tos --email "$LETSENCRYPT_MAIL" --preferred-challenges http-01,dns-01 --rsa-key-size 4096 --cert-name $certname $DRY $FORCE --deploy-hook /etc/letsencrypt/renewal-hooks.d/$certname &> /var/log/ssl.letsencrypt.renew.$certname )  
        test -f /etc/letsencrypt/renewal-hooks.d/$certname  || (  echo " no deploy hook ++";
                /usr/bin/certbot renew --no-random-sleep-on-renew --agree-tos --email "$LETSENCRYPT_MAIL" --preferred-challenges http-01,dns-01 --rsa-key-size 4096 --cert-name $certname $DRY $FORCE  &> /var/log/ssl.letsencrypt.renew.$certname )
done

